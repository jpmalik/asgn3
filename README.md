# asgn3

This program checks your passwords to make sure if they are valid

Files:
- .gitignore - Ignore compilation files and executables
- Makefile - compilation file for compiling and running code 
- password_checker.c - main program for checking passwords
- password_checker.h - header file for c file
- test_password_checker.c - program test file